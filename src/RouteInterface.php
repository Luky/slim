<?php declare(strict_types = 1);

namespace Luky\Slim;

interface RouteInterface
{
	public function create(\Slim\App $app): void;
}
