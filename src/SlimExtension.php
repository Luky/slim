<?php declare(strict_types = 1);

namespace Luky\Slim;

use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;
use Slim\App;

class SlimExtension extends CompilerExtension
{
	private const DEFAULT
		= [
			'default'  => null,
			'routes'   => [],
			'settings' => [
				'debug'                  => true,
				'displayErrorDetails'    => true,
				'addContentLengthHeader' => false,
			],
		];
	public const ROUTE_TAG = 'route';
	public const ROUTE_TAG_DEFAULT = 'defaultRoute';
	
	
	/** @var string[] */
	private $routes;
	/** @var mixed */
	private $default;
	/** @var mixed[] slimSettings */
	private $settings;
	
	/**
	 * @throws \Luky\Slim\RouteException
	 */
	public function loadConfiguration(): void
	{
		$config = $this->getConfig(self::DEFAULT);
		
		$this->routes = $config['routes'];
		$this->default = $config['default'];
		
		$this->settings = $config['settings'];
		
		if ($this->routes === []) {
			throw new RouteException('No `routes` defined.');
		}
		
		if ($this->default === null) {
			throw new RouteException('No `default` route defined.');
		}
	}
	
	/**
	 * @throws \Luky\Slim\CompileException
	 * @throws \Nette\InvalidArgumentException
	 * @throws \Nette\InvalidStateException
	 */
	public function beforeCompile(): void
	{
		$builder = $this->getContainerBuilder();
		
		// ---------------------------------
		// Register Slim/App service
		// ---------------------------------
		$slimAppDefinition = $builder->addDefinition($this->prefix('slimApp'));
		$slimAppDefinition->setType(App::class);
		$slimAppDefinition->setArguments([$this->settings]);
		
		
		$sourceDir = $builder->parameters['sourceDir'];
		
		// ---------------------------------
		// Register Routes
		// ---------------------------------
		
		/**
		 * @var int                 $i
		 * @var \Nette\DI\Statement $route
		 */
		foreach ($this->routes as $i => $route) {
			
			if (\class_exists($route) === false) {
				throw new CompileException("Class {$route} doesn't exists.");
			}
			
			$interfaces = ClassType::from($route)->getImplements();
			
			if (\in_array(RouteInterface::class, $interfaces, true) === false) {
				throw new CompileException("Class {$route} must implement '" . RouteInterface::class . "'");
			}
			
			$serviceDefinition = $builder
				->addDefinition($this->prefix("routes.$i"));
			
			if (\is_string($route)) {
				
				$serviceDefinition->setType($route);
				$slimAppDefinition->addSetup("@{$route}::create");
			} else {
				
				$serviceDefinition->setType($route->getEntity(), $route->arguments);
				$slimAppDefinition->addSetup("@{$route->getEntity()}::create");
			}
		}
	}
}
