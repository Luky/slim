<?php declare(strict_types = 1);

namespace Luky\Slim;

class RouteException extends \LogicException
{
}
