

## Example Config in neon (Nette DI)

```neon
extensions:
	route: Luky\Slim\SlimExtension

route:
	default: MyUberApp\Route\DefaultRoute
	routes:
		- MyUberApp\Route\ArticleRoute
		- MyUberApp\Route\UserRoute
```

## Example PHP Wrapper
```php
<?php declare(strict_types = 1);

namespace MyUberApp\Route;

class ExampleRoute
{
	/** @var \ExampleFacade */
	private $exampleFacade;
	
	public function __construct(\ExampleFacade $exampleFacade)
	{
		$this->exampleFacade = $exampleFacade;
	}
	
	public function create(\Slim\App $app): void
	{
		$deps = $this;
		
		$app->group(
			'/article',
			function () use ($app, $deps) {
				
				$app->get(
					'/feed',
					function (\Slim\Http\Request $req, \Slim\Http\Response $res) use ($deps) {
						
						return $res->withJson(
							$deps->exampleFacade->feed()
						);
					}
				);
			}
		);
	}
}
```
